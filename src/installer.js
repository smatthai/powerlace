const childProcess = require('child_process');

// TODO check if brew / mas is installed
const install = app => {
  if (!app || !app.type || !app.id) {
    return null;
  }
  switch (app.type) {
    case 'brew-cask': {
      childProcess.execSync(`brew cask install ${app.id}`, { stdio: 'inherit' });
      return app;
    }
    case 'brew': {
      childProcess.execSync(`brew install ${app.id}`, { stdio: 'inherit' });
      return app;
    }
    case 'mas': {
      childProcess.execSync(`mas install ${app.id}`, { stdio: 'inherit' });
      return app;
    }
    default:
      console.warn(`Could not install app [${app.id}] of type [${app.type}].`);
      return null;
  }
}

const uninstall = app => {
  if (!app || !app.type || !app.id) {
    return null;
  }
  switch (app.type) {
    case 'brew-cask': {
      childProcess.execSync(`brew cask uninstall ${app.id}`, { stdio: 'inherit' });
      return app;
    }
    case 'brew': {
      childProcess.execSync(`brew uninstall ${app.id}`, { stdio: 'inherit' });
      return app;
    }
    case 'mas': {
      childProcess.execSync(`sudo mas uninstall ${app.id}`, { stdio: 'inherit' });
      return app;
    }
    default:
      console.warn(`Could not uninstall app [${app.id}] of type [${app.type}].`);
      return null;
  }
}

module.exports = {
  install,
  uninstall,
};
