const diffById = (a = [], b = []) => {
  if (!Array.isArray(b)) {
    return Array.isArray(a) ? a : [];
  } else if (!Array.isArray(a)) {
    return [];
  }

  return a.reduce((acc, aItem) => {
    if (!b.find(bItem => bItem.id === aItem.id)) {
      acc.push(aItem);
    }
    return acc;
  }, []);
}

module.exports = diffById;
