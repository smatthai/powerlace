# powerlace
Install mac applications via a config that can be source controlled!

> Power laces, alright!

## Prerequisites
In order to use powerlace, you must have brew and mas installed. If you have not installed these, you can run the following:
```
curl -o- https://gitlab.com/jedfong/powerlace/raw/master/bin/install-prerequisites.sh | bash
```

## Usage
Currently powerlace does not support installing apps via the cli. Until support for that is added you will need to create a `~/.powerlace/config.js`.

Once your config is created, you can run:
```
npx powerlace
```

## Configuration
The following is an example config.
```javascript
// ~/.powerlace/config.js
module.exports = {
  apps: [
     { type: 'brew', id: 'lynx' },
     { type: 'brew', id: 'wget' },
     { type: 'brew-cask', id: 'atom' },
     // { type: 'brew-cask', id: 'keepingyouawake' },
     { type: 'mas', id: '880001334', name: 'Reeder 3'},
  ]
};
```
_Note that you can comment out apps you don't want installed. If any apps are removed from the config or commented out, they will be uninstalled the next time powerlace is run._

## Roadmap
- [ ] Add ability to install apps via powerlace cli
- [ ] Add ability to uninstall apps via powerlace cli
- [ ] Show less noise when installing apps
- [ ] Show progress indicator when installing apps
