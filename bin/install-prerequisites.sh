#!/bin/bash
if [[ ! -e ~/.bashrc ]]; then
  touch ~/.bashrc && chmod 600 ~/.bashrc
fi

if [[ ! -e ~/.bash_profile ]]; then
  echo "#!/bin/bash" > ~/.bash_profile
  echo "[[ -s ~/.bashrc ]] && source ~/.bashrc" >> ~/.bash_profile
  chmod 600 ~/.bash_profile
fi

which node > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo Installing nvm...
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash

  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
  echo Installing node...
  nvm install node
fi

which brew > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo Installing brew...
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

brew info cask > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo Installing brew cask...
  brew tap caskroom/cask
fi

which mas > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo Installing mas...
  brew install mas
fi

echo Complete the install by opening a new terminal session
